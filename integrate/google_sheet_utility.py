import logging
import time
import gspread
from google.oauth2.service_account import Credentials
from gspread import Cell


class GoogleSheetUtility:
    def __init__(self, spreadsheet_key):
        self.header_name = []
        self.cols_name = []
        self.scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
        self.creds = Credentials.from_service_account_file("integrate/service_account.json", scopes=self.scope)
        self.client = gspread.authorize(self.creds)
        self.google_sh = self.client.open_by_key(spreadsheet_key)

    def convert_to_dict(self, data: list):
        while len(data) < len(self.header_name):
            data.append("")
        return dict(zip(self.header_name, data))

    def get_all_records(self):
        return self.google_sh.sheet1.get_all_records()

    def find_first_cell_by(self, col_name, query) -> Cell:
        in_column = self.header_name.index(col_name) + 1
        return self.google_sh.sheet1.find(query=query, in_column=in_column)

    def findall_cell_by(self, col_name, query) -> list[Cell]:
        in_column = self.header_name.index(col_name) + 1
        return self.google_sh.sheet1.findall(query=query, in_column=in_column)

    def get_record_by_cell(self, cell: Cell):
        row_values = self.google_sh.sheet1.row_values(cell.row)
        return self.convert_to_dict(row_values)

    def find_first_record_by(self, h_col_name, query):
        """
        find first record
        :param query:
        :param h_col_name:
        """
        logging.info(f"find_first_record_by: h_col_name={h_col_name}, query={query}")
        cell = self.find_first_cell_by(h_col_name, query)
        if cell is None:
            return dict(zip(self.header_name))

        row_values = self.google_sh.sheet1.row_values(cell.row)
        return self.convert_to_dict(row_values)

    def findall_records_by(self, h_col_name, query):
        """
        find all records by filter data in cell of header name
        :param query:
        :param h_col_name: ex: machine_name
        """
        logging.info(f"findall_records_by: h_col_name={h_col_name}, query={query}")
        cells = self.findall_cell_by(h_col_name, query)
        if cells is None:
            return dict(zip(self.header_name))
        results = []
        for cell in cells:
            row_values = self.google_sh.sheet1.row_values(cell.row)
            results.append(self.convert_to_dict(row_values))

        return results

    def insert_new_row(self, data: dict):
        """add new row to google sheet
        :param data: data to add
        ex:
        {
            "machine_name": "TEST",
            "model": "2024"
        }
        """
        logging.info(f"insert_new_row data is {data}")
        values = [str(int(time.time() * 1000))] + [data.get(key, "") for key in self.header_name[1:]]
        self.google_sh.sheet1.append_row(values)

    def update_cell_by(self, row_num, col_name, value):
        """
        update a single cell by header name and row index
        """
        logging.info(f"update_cell_by col_name={col_name}, value={value}, row_num={row_num}")
        cell_col_label = self.cols_name[self.header_name.index(col_name)]
        cell_label = f"{cell_col_label}{row_num}"
        self.google_sh.sheet1.update_acell(cell_label, value)

    def update_multiple_cell_by(self, row_num: int, data: dict):
        """
        update multiple column in row
        :param row_num: row index
        :param data: data to update, ex: {machine_name: "test"}
        """
        logging.info(f"update_multiple_cell_by row_num: {row_num}, data is: {data}")
        cell_range: list[Cell] = self.google_sh.sheet1.range(
            f"{self.cols_name[1]}{row_num}:{self.cols_name[-1]}{row_num}")
        cell_list = []
        for i, cell in enumerate(cell_range):
            for key, value in data.items():
                if cell.col == self.header_name.index(key) + 1:
                    cell.value = value
                    cell_list.append(cell)
        self.google_sh.sheet1.update_cells(cell_list)
