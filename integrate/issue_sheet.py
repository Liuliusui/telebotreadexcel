from config.config import GSHEET_ISSUE_ID
from integrate.google_sheet_utility import GoogleSheetUtility


class IssueSheet(GoogleSheetUtility):
    def __init__(self):
        super().__init__(spreadsheet_key=GSHEET_ISSUE_ID)
        self.header_name = ["id", "id_message", "imei", "date_created",
                            "date_modified", "status",
                            "location", "location_check", "images", "report", "staff", "note"]
        self.cols_name = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"]

    def make_dict_data(
            self,
            imei=None,
            id_message=None,
            date_created=None,
            date_modified=None,
            status=None,
            location=None,
            location_check=None,
            images=None,
            report=None,
            staff=None,
            note=None,
    ) -> dict:
        """
        {
            "imei": imei,
            "date_created": date_created,
            "date_modified": date_modified,
            "status": status,
            "location": location,
            "location_check": location_check,
            "images": images,
            "staff": staff,
            "note": note,
        }
        """
        data = {}
        if imei is not None:
            data["imei"] = imei
        if id_message is not None:
            data["id_message"] = id_message
        if date_created is not None:
            data["date_created"] = date_created
        if date_modified is not None:
            data["date_modified"] = date_modified
        if status is not None:
            data["status"] = status
        if location is not None:
            data["location"] = location
        if location_check is not None:
            data["location_check"] = location_check
        if images is not None:
            data["images"] = images
        if report is not None:
            data["report"] = report
        if staff is not None:
            data["staff"] = staff
        if note is not None:
            data["note"] = note
        return data
