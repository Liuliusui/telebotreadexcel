from config.config import GSHEET_MACHINE_ID
from integrate.google_sheet_utility import GoogleSheetUtility


class MachineSheet(GoogleSheetUtility):
    def __init__(self):
        super().__init__(spreadsheet_key=GSHEET_MACHINE_ID)
        self.header_name = ["id", "id_message", "imei", "machine_name", "model", "customer_name", "customer_phone",
                            "customer_location", "customer_location_text", "images", "note"]
        self.cols_name = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K"]

    def make_dict_data(
            self,
            id_message=None,
            imei=None,
            machine_name=None,
            model=None,
            customer_name=None,
            customer_phone=None,
            customer_location=None,
            customer_location_text=None,
            images=None,
            note=None
    ) -> dict:
        """
        {
            "imei": imei,
            "machine_name": machine_name,
            "model": model,
            "customer_name": customer_name,
            "customer_phone": customer_phone,
            "customer_location": customer_location,
            "customer_location_text": customer_location_text,
            "images": images,
            "note": note
        }
        """
        data = {}
        if imei is not None:
            data["imei"] = imei
        if id_message is not None:
            data["id_message"] = id_message
        if machine_name is not None:
            data["machine_name"] = machine_name
        if model is not None:
            data["model"] = model
        if customer_name is not None:
            data["customer_name"] = customer_name
        if customer_phone is not None:
            data["customer_phone"] = customer_phone
        if customer_location is not None:
            data["customer_location"] = customer_location
        if customer_location_text is not None:
            data["customer_location_text"] = customer_location_text
        if images is not None:
            data["images"] = images
        if note is not None:
            data["note"] = note

        return data
