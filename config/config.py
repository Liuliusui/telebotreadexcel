import logging
import os
import sys
from datetime import datetime
from logging.handlers import TimedRotatingFileHandler

from dotenv import load_dotenv
from pytz import timezone

from config.custom_formatter import CustomFormatter

load_dotenv()

# get config from env
BOT_TOKEN = os.environ.get('BOT_TOKEN')
LOG_FILE = os.environ.get('LOG_FILE')
GSHEET_MACHINE_ID = os.environ.get('GSHEET_MACHINE_ID')
GSHEET_ISSUE_ID = os.environ.get('GSHEET_ISSUE_ID')


# config logger
os.makedirs(os.path.dirname(LOG_FILE), exist_ok=True)

logging.Formatter.converter = lambda *args: datetime.now(tz=timezone('Asia/Ho_Chi_Minh')).timetuple()
fmt = '%(levelname)s %(asctime)s: %(filename)s %(lineno)d: %(message)s'
stream_handler = logging.StreamHandler(sys.stdout)
stream_handler.setFormatter(CustomFormatter(fmt))

logging.basicConfig(
    level=logging.DEBUG,
    datefmt='%Y-%m-%d %H:%M:%S %Z',
    format=fmt,
    encoding='utf-8',
    handlers=[
        TimedRotatingFileHandler(filename=LOG_FILE, when='midnight', backupCount=30, encoding='utf-8'),
        stream_handler
    ],
)
