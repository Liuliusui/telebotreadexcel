from helpers.strings import ProductErrorReporting


class LocationModel:

    def __init__(self):
        self.latitude = 0.0
        self.longitude = 0.0

    def to_string(self):
        return ProductErrorReporting.LOCATION_STRING.value.format(self.latitude, self.longitude)


class ProductModel:
    imei = ""
    customer_name = ""
    phone_number = ""
    location = LocationModel()
    description = ""

    def __init__(self):
        self.imei = ""
        self.customer_name = ""
        self.phone_number = ""
        self.location = LocationModel()
        self.description = ""
