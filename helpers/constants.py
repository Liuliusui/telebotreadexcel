from enum import Enum


class MessageHandlerCommands(Enum):
    REPORT_ISSUE_START = "report"
    REPORT_ISSUE_START_1 = "issue"
    ADD_PRODUCT_START = "add"
    ADD_PRODUCT_START_1 = "menu"
    START = "start"
    HELLO = "hello"
    READ_RECORD = "read_record"
    WRITE_RECORD = "write_record"
    SEND_LOCATION = "send_location"
    PRODUCT_ERROR_REPORT = "error_report"
    CANCEL = "cancel"
    SKIP = "skip"


class MessageSendContent(Enum):
    SUCCEED = "success"


class ChannelId(Enum):
    REPORT_ISSUE = "test_bot_long"
    ADD_NEW_PRODUCT = "newproduct9499"


class AllowedUsersCommands(Enum):
    SET_ADMIN_ID = "set_admin_id"
