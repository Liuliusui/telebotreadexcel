from enum import Enum


class ProductErrorReporting(Enum):
    GREETING = "Hi! I'm a conversation bot. Please choose an option:"
    GET_CUSTOMER_DATA = "Please provide customer's name"
    GET_PHONE_NUMBER = "Please provide customer's phone number"
    GET_LOCATION = "Please provide customer's location"
    GET_DESCRIPTION = "Please provide issue's description"
    END_CONVERSATION = "Your report will be send to employee"
    REPORT_SEND_MESSAGE = ("You have 1 issue to solved\n"
                           "Imei: {} \n"
                           "Customer: {} with phone number: {}\n"
                           "Location: {} ({})\n"
                           "Description: {}\n")
    LOCATION_STRING = "{}, {}"
    CONFIRM_MESSAGE = "Are you sure to report this issue?\n"


class AddProduct(Enum):
    GET_PRODUCT_IMEI = "Please provide product's imei"
    GET_PRODUCT_NAME = "Please provide product's name"
    GET_PRODUCT_MODEL = "Please provide product's model"
    GET_CUSTOMER_NAME = "Please provide customer's name"
    GET_CUSTOMER_PHONE_NUMBER = "Please provide customer's phone number"
    GET_CUSTOMER_LOCATION_COORDINATE = "Please provide product's location with coordinate or /skip"
    GET_CUSTOMER_LOCATION_DESC = "Please provide product's location with description"
    GET_CUSTOMER_IMAGES = "Please provide product's images or /skip"
    GET_PRODUCT_NOTE = "Please provide product's note or /skip"  # optional
    END_CONVERSATION = "Your new data is saving"
    PRODUCT_STRING = "Imei: {} ~ Name: {}"
    ADD_NEW_PRODUCT_MESSAGE = ("A new product has been added\n"
                               "Imei: {}\n"
                               "Product: {} with model: {}\n"
                               "Customer: {} with phone number: {}\n"
                               "Location: {}")


class MessageObserver(Enum):
    LOCATION_CHECKED = "Location checked"


class LimitedAccess(Enum):
    ADD_NEW_USER = "Type new username you want to add to access"
    USER_NOT_ALLOWED = "Sorry, you are not allowed access this conversation. Please contact admin"
    USER_HAS_ACCESS = "User {} has been given access permissions"


class Cancel(Enum):
    CANCEL_CONVERSATION = "Cancel conversation"
