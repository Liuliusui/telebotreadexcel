from commands.menu.model.new_issue_model import NewIssueModel
from commands.menu.model.new_product_model import NewProductModel
from commands.product_error_reporting.send_report_to_user import send_new_product_message
from service.product_service import save_new_product_to_server, save_new_issue_to_server


async def save_new_product(new_product: NewProductModel = NewProductModel()) -> int:
    sent_message_id = await send_new_product_message(new_product=new_product)
    new_product.sent_message_id = sent_message_id
    await save_new_product_to_server(new_product)
    return 1


async def save_new_issue(report_data: NewIssueModel = NewIssueModel()) -> int:
    await save_new_issue_to_server(report_data)
    return 1
