from helpers.strings import AddProduct
from model.product_model import ProductModel


class NewProductModel(ProductModel):
    sent_message_id = ""
    name = ""
    model = ""
    location_desc = ""
    images = ""
    note = ""

    def __init__(self):
        super().__init__()
        self.sent_message_id = 0
        self.name = ""
        self.model = ""
        self.location_desc = ""
        self.images = ""
        self.note = ""

    def to_string(self):
        return AddProduct.PRODUCT_STRING.value.format(self.imei, self.name)
