from enum import Enum

from model.product_model import ProductModel


class IssueStatus(Enum):
    PENDING = "pending"
    COMPLETE = "complete"


class LocationStatus(Enum):
    INSIDE = "inside"
    OUTSIDE = "outside"


class NewIssueModel(ProductModel):
    sent_message_id = ""
    customer_phone = ""
    date_created = ""
    date_modified = ""
    status = ""
    location_check = ""
    location_description = ""
    images = ""
    staff = ""
    note = ""
    location = ""

    def __init__(self):
        super().__init__()
        self.sent_message_id = 0
        self.customer_phone = ""
        self.date_created = ""
        self.date_modified = ""
        self.status = IssueStatus.PENDING.value
        self.location_check = LocationStatus.OUTSIDE.value
        self.location_description = ""
        self.images = ""
        self.staff = ""
        self.note = ""
        self.location = ""
