from enum import Enum

from telegram import ReplyKeyboardRemove, ReplyKeyboardMarkup
from telegram.ext import (CommandHandler, MessageHandler, filters, CallbackContext)
from telegram import Update
from telegram.ext import ContextTypes, ConversationHandler

from config.config import logging as logger
from helpers.constants import MessageHandlerCommands
from helpers.strings import AddProduct, ProductErrorReporting, LimitedAccess
from integrate.sample import machine
from commands.limited_access.limited_access import allowed_users
from .model.new_issue_model import NewIssueModel
from .model.new_product_model import NewProductModel
from .menu_service import save_new_product, save_new_issue
from ..common.cancel import cancel
from model.product_model import LocationModel
from ..product_error_reporting.send_report_to_user import send_new_issue_message

(IMEI, NAME, MODEL, CUSTOMER_NAME, CUSTOMER_PHONE_NUMBER, LOCATION_COORDINATE, LOCATION_DESCRIPTION, IMAGES, NOTE,
 IMEI_ISSUE, DESCRIPTION, CHOOSING, CHOOSING_CONFIRM) = range(13)

report_data = NewIssueModel()
new_product = NewProductModel()


class MenuButton(Enum):
    ADD_NEW_PRODUCT = 'Add new Product'
    REPORT_AN_ISSUE = 'Report an issue'


class IssueConfirmButton(Enum):
    CONFIRM = 'Ok'
    NOT_CONFIRM = 'Cancel'


async def start(update: Update, context: CallbackContext) -> int:
    if update.effective_message.from_user.username not in allowed_users:
        await update.message.reply_text(LimitedAccess.USER_NOT_ALLOWED.value)
        return -1
    reply_keyboard = [[MenuButton.ADD_NEW_PRODUCT.value, MenuButton.REPORT_AN_ISSUE.value]]
    await update.message.reply_text(
        ProductErrorReporting.GREETING.value,
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
    )
    return CHOOSING


# Define a function to handle the user's choice
async def user_choice(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    if text == MenuButton.ADD_NEW_PRODUCT.value:
        await update.message.reply_text(
            AddProduct.GET_PRODUCT_IMEI.value
        )
        return IMEI
    elif text == MenuButton.REPORT_AN_ISSUE.value:
        await update.message.reply_text(
            AddProduct.GET_PRODUCT_IMEI.value
        )
        return IMEI_ISSUE


async def get_imei(update: Update, context=ContextTypes.DEFAULT_TYPE) -> int:
    """Get product's imei and ask about product's name."""
    new_product.imei = update.message.text
    user = update.message.from_user
    logger.info("User %s add product with imei: %s", user.first_name, update.message.text)
    await update.message.reply_text(
        AddProduct.GET_PRODUCT_NAME.value
    )
    return NAME


async def get_name(update: Update, context=ContextTypes.DEFAULT_TYPE) -> int:
    """Get product's name and ask about product's model."""
    new_product.name = update.message.text
    user = update.message.from_user
    logger.info("User %s add product with name: %s", user.first_name, update.message.text)
    await update.message.reply_text(
        AddProduct.GET_PRODUCT_MODEL.value
    )
    return MODEL


async def get_model(update: Update, context=ContextTypes.DEFAULT_TYPE) -> int:
    """Get product's model and ask about customer's name."""
    new_product.model = update.message.text
    user = update.message.from_user
    logger.info("User %s add product with model: %s", user.first_name, update.message.text)
    await update.message.reply_text(
        AddProduct.GET_CUSTOMER_NAME.value
    )
    return CUSTOMER_NAME


async def get_customer_name(update: Update, context=ContextTypes.DEFAULT_TYPE) -> int:
    """Get customer's name and ask about customer's phone number."""
    new_product.customer_name = update.message.text
    user = update.message.from_user
    logger.info("User %s's name is : %s", user.first_name, update.message.text)
    await update.message.reply_text(
        AddProduct.GET_CUSTOMER_PHONE_NUMBER.value
    )
    return CUSTOMER_PHONE_NUMBER


async def get_customer_phone_number(update: Update, context=ContextTypes.DEFAULT_TYPE) -> int:
    """Get customer's phone number and ask about customer's location."""
    new_product.phone_number = update.message.text
    user = update.message.from_user
    logger.info("User %s's phone number is : %s", user.first_name, update.message.text)
    await update.message.reply_text(
        AddProduct.GET_CUSTOMER_LOCATION_COORDINATE.value
    )
    return LOCATION_COORDINATE


async def get_location_coordinate(update: Update, context=ContextTypes.DEFAULT_TYPE) -> int:
    """Get customer's location coordinate and ask about customer's location desc."""
    user = update.message.from_user
    user_location = update.message.location
    location_model = LocationModel()
    location_model.latitude = user_location.latitude
    location_model.longitude = user_location.longitude
    new_product.location = location_model
    logger.info(
        "Location of %s: %f / %f", user.first_name, user_location.latitude, user_location.longitude
    )
    await update.message.reply_text(
        AddProduct.GET_CUSTOMER_LOCATION_DESC.value
    )
    return LOCATION_DESCRIPTION


async def skip_get_location_coordinate(update: Update, context=ContextTypes.DEFAULT_TYPE) -> int:
    await update.message.reply_text(
        AddProduct.GET_CUSTOMER_LOCATION_DESC.value
    )
    return LOCATION_DESCRIPTION


async def get_location_desc(update: Update, context=ContextTypes.DEFAULT_TYPE) -> int:
    """Get customer's location desc and ask about product's images"""
    new_product.location_desc = update.message.text
    user = update.message.from_user
    logger.info("User %s's location description is : %s", user.first_name, update.message.text)
    await update.message.reply_text(
        AddProduct.GET_CUSTOMER_IMAGES.value
    )
    return IMAGES


async def get_images(update: Update, context=ContextTypes.DEFAULT_TYPE) -> int:
    """Get product's images and ask about product's note"""
    user = update.message.from_user
    photo_file = await update.message.photo[-1].get_file()
    # path = await photo_file.download_to_drive("user_photo_{}.jpg".format(time.time()))
    new_product.images = photo_file.file_path
    logger.info("Photo of %s: %s", user.first_name, photo_file.file_path)
    await update.message.reply_text(
        AddProduct.GET_PRODUCT_NOTE.value
    )
    return NOTE


async def skip_get_images(update: Update, context=ContextTypes.DEFAULT_TYPE) -> int:
    await update.message.reply_text(
        AddProduct.GET_PRODUCT_NOTE.value
    )
    return NOTE


async def get_note(update: Update, context=ContextTypes.DEFAULT_TYPE) -> int:
    """Get product's note and end conversation"""
    new_product.note = update.message.text
    user = update.message.from_user
    logger.info("User %s add product note is : %s", user.first_name, update.message.text)
    await update.message.reply_text(
        AddProduct.END_CONVERSATION.value
    )
    await save_new_product(new_product)
    return ConversationHandler.END


async def skip_note(update: Update, context=ContextTypes.DEFAULT_TYPE) -> int:
    """Skip get product's note and end conversation"""
    await update.message.reply_text(
        AddProduct.END_CONVERSATION.value
    )
    await save_new_product(new_product)
    return ConversationHandler.END


# -------- #

async def get_imei_issue(update: Update, context=ContextTypes.DEFAULT_TYPE) -> int:
    """Get product's imei and ask about product's name."""
    report_data.imei = update.message.text
    user = update.message.from_user
    logger.info("User %s add product with imei: %s", user.first_name, update.message.text)
    await update.message.reply_text(
        ProductErrorReporting.GET_DESCRIPTION.value
    )
    return DESCRIPTION


async def get_description(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Get error's description and end conversation."""
    user = update.message.from_user
    report_data.description = update.message.text
    logger.info("User %s canceled the conversation.", user.first_name)
    reply_keyboard = [[IssueConfirmButton.CONFIRM.value, IssueConfirmButton.NOT_CONFIRM.value]]
    machine_cell = machine.find_first_cell_by("imei", report_data.imei)
    if machine_cell is None:
        return
    machine_record = machine.get_record_by_cell(machine_cell)
    report_data.location = machine_record.get("customer_location")
    report_data.customer_name = machine_record.get("customer_name")
    report_data.location_description = machine_record.get("customer_location_text")
    await update.message.reply_text(
        ProductErrorReporting.CONFIRM_MESSAGE.value + ProductErrorReporting.REPORT_SEND_MESSAGE.value.format(
            report_data.imei,
            report_data.customer_name,
            report_data.phone_number,
            report_data.location,
            report_data.location_description,
            report_data.description),
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
    )
    return CHOOSING_CONFIRM


async def check_issue_confirm(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    if text == IssueConfirmButton.NOT_CONFIRM.value:
        return ConversationHandler.END
    else:
        await update.message.reply_text(
            ProductErrorReporting.END_CONVERSATION.value, reply_markup=ReplyKeyboardRemove()
        )
        await save_new_issue_to_server()
        return ConversationHandler.END


async def save_new_issue_to_server():
    sent_message_id = await send_new_issue_message(report_data=report_data)
    report_data.sent_message_id = sent_message_id
    await save_new_issue(report_data=report_data)


add_new_product_handler = ConversationHandler(
    entry_points=[CommandHandler(MessageHandlerCommands.START.value, start)],
    states={
        CHOOSING: [MessageHandler(filters.TEXT & ~filters.COMMAND, user_choice)],
        IMEI: [MessageHandler(filters.TEXT & ~filters.COMMAND, get_imei)],
        NAME: [MessageHandler(filters.TEXT & ~filters.COMMAND, get_name)],
        MODEL: [MessageHandler(filters.TEXT & ~filters.COMMAND, get_model)],
        CUSTOMER_NAME: [MessageHandler(filters.TEXT & ~filters.COMMAND, get_customer_name)],
        CUSTOMER_PHONE_NUMBER: [MessageHandler(filters.TEXT & ~filters.COMMAND, get_customer_phone_number)],
        LOCATION_COORDINATE: [MessageHandler(filters.LOCATION & ~filters.COMMAND, get_location_coordinate),
                              CommandHandler(MessageHandlerCommands.SKIP.value, skip_get_location_coordinate)],
        LOCATION_DESCRIPTION: [MessageHandler(filters.TEXT & ~filters.COMMAND, get_location_desc)],
        IMAGES: [MessageHandler(filters.PHOTO & ~filters.COMMAND, get_images),
                 CommandHandler(MessageHandlerCommands.SKIP.value, skip_get_images)],
        NOTE: [MessageHandler(filters.TEXT & ~filters.COMMAND, get_note),
               CommandHandler(MessageHandlerCommands.SKIP.value, skip_note)],

        IMEI_ISSUE: [MessageHandler(filters.TEXT & ~filters.COMMAND, get_imei_issue)],
        DESCRIPTION: [MessageHandler(filters.TEXT & ~filters.COMMAND, get_description)],
        CHOOSING_CONFIRM: [MessageHandler(filters.TEXT & ~filters.COMMAND, check_issue_confirm)],
    },
    fallbacks=[CommandHandler(MessageHandlerCommands.CANCEL.value, cancel)],
)
