from telegram import Update
from telegram.ext import (CommandHandler, CallbackContext, MessageHandler, filters)
from telegram.ext import ConversationHandler

from helpers.constants import MessageHandlerCommands, AllowedUsersCommands
from helpers.strings import LimitedAccess
from ..common.cancel import cancel

admin_users = ["lamisme99", "Ln9499"]

allowed_users = ["lamisme99", "Ln9499"]

(GET_ADMIN_ID) = range(2)


async def set_admin_id(update: Update, context: CallbackContext) -> int:
    if update.effective_message.from_user.username not in admin_users:
        await update.message.reply_text(LimitedAccess.USER_NOT_ALLOWED.value)
        return -1
    await update.message.reply_text(
        LimitedAccess.ADD_NEW_USER.value
    )
    return GET_ADMIN_ID


async def get_admin_id(update: Update, context: CallbackContext) -> int:
    new_allowed_user = update.message.text
    allowed_users.append(new_allowed_user)
    message = LimitedAccess.USER_HAS_ACCESS.value.format(new_allowed_user)
    await update.message.reply_text(
        message
    )
    return ConversationHandler.END


set_allowed_user_handler = ConversationHandler(
    entry_points=[CommandHandler(AllowedUsersCommands.SET_ADMIN_ID.value, set_admin_id)],
    states={
        GET_ADMIN_ID: [MessageHandler(filters.TEXT & ~filters.COMMAND, get_admin_id)],
    },
    fallbacks=[CommandHandler(MessageHandlerCommands.CANCEL.value, cancel)],
)
