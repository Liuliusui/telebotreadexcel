from telegram import Update, ReplyKeyboardRemove
from telegram.ext import ContextTypes, ConversationHandler
from config.config import logging as logger
from helpers.strings import Cancel


async def cancel(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Cancels and ends the conversation."""
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    await update.message.reply_text(
        Cancel.CANCEL_CONVERSATION.value, reply_markup=ReplyKeyboardRemove()
    )

    return ConversationHandler.END
