from commands.message_observer.utils import get_download_link
from config import BOT_TOKEN
from config.config import logging as logger
from helpers.strings import ProductErrorReporting
from integrate.sample import machine


async def on_reply_add_new_product_message(message):
    original_message = message.reply_to_message
    chat_id = original_message.forward_from_message_id
    # find issue by chat_id
    issue_cell = machine.find_first_cell_by("id_message", str(chat_id))
    if issue_cell is None:
        return
    logger.info(f"------Channel------ Chat ID: {chat_id}")
    if message.photo:
        file_id = message.photo[-1].file_id
        logger.info(f"------Channel------ Photo: {file_id}.jpg")
        download_link = get_download_link(BOT_TOKEN, file_id)
        machine.update_cell_by(row_num=issue_cell.row, col_name="images", value=download_link)
    # Check if the message is location
    elif message.location:
        user_location = message.location
        latitude = user_location.latitude
        longitude = user_location.longitude
        location = ProductErrorReporting.LOCATION_STRING.value.format(latitude, longitude)
        machine.update_cell_by(row_num=issue_cell.row, col_name="customer_location", value=location)
        # Convert the substrings to float numbers
        logger.info(f"------Channel------ Location: {latitude} {longitude}")
    elif message.text:
        machine.update_cell_by(row_num=issue_cell.row, col_name="note", value=message.text)
