import requests
from config.config import logging as logger


def get_download_link(bot_token, file_id):
    url = f"https://api.telegram.org/bot{bot_token}/getFile"
    params = {'file_id': file_id}
    response = requests.get(url, params=params)
    if response.status_code == 200:
        file_path = response.json()['result']['file_path']
        file_url = f"https://api.telegram.org/file/bot{bot_token}/{file_path}"
        return file_url
    else:
        logger.error("Failed to download photo.")
