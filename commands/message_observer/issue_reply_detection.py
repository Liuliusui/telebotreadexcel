from datetime import datetime
from math import radians, sin, cos, sqrt, atan2

from commands.menu.model.new_issue_model import IssueStatus
from commands.message_observer.utils import get_download_link
from config.config import logging as logger

from config import BOT_TOKEN
from helpers.strings import ProductErrorReporting, MessageObserver
from integrate.sample import repair


def is_in_allowed_radius(lat1, lon1, lat2, lon2):
    # Convert latitude and longitude from degrees to radians
    lat1, lon1, lat2, lon2 = map(radians, [lat1, lon1, lat2, lon2])

    # Haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    distance = 6371 * c  # Radius of the Earth in kilometers
    return distance <= 1


async def on_reply_issue_message(update, message):
    original_message = message.reply_to_message
    chat_id = original_message.forward_from_message_id
    # find issue by chat_id
    issue_cell = repair.find_first_cell_by("id_message", str(chat_id))
    if issue_cell is None:
        return
    issue_record = repair.get_record_by_cell(issue_cell)
    images = issue_record.get("images")
    # update date_modified
    current_time = datetime.now()
    # Format the current time as a string
    date_modified = current_time.strftime("%Y-%m-%d %H:%M:%S")
    repair.update_cell_by(row_num=issue_cell.row, col_name="date_modified", value=date_modified)
    # update staff
    repair.update_cell_by(row_num=issue_cell.row, col_name="staff", value=original_message.from_user.first_name)
    logger.info(f"------Channel------ Chat ID: {chat_id}")
    if message.reply_to_message and message.photo:
        file_id = message.photo[-1].file_id
        logger.info(f"------Channel------ Photo: {file_id}.jpg")
        download_link = get_download_link(BOT_TOKEN, file_id)
        if message.caption:
            # update report image
            repair.update_cell_by(row_num=issue_cell.row, col_name="report", value=download_link)
            # update issue status
            repair.update_cell_by(row_num=issue_cell.row, col_name="status",
                                  value=IssueStatus.COMPLETE.value)
        else:
            images += download_link + ", "
            repair.update_cell_by(row_num=issue_cell.row, col_name="images", value=images)
    # Check if the message is location
    elif message.location:
        user_location = message.location
        latitude = user_location.latitude
        longitude = user_location.longitude
        location_check = ProductErrorReporting.LOCATION_STRING.value.format(latitude, longitude)
        repair.update_cell_by(row_num=issue_cell.row, col_name="location_check", value=location_check)
        issue_location = issue_record.get("location")
        coordinates_list = issue_location.split(', ')
        # Convert the substrings to float numbers
        issue_latitude = float(coordinates_list[0])
        issue_longitude = float(coordinates_list[1])
        if is_in_allowed_radius(latitude, longitude, issue_latitude, issue_longitude):
            await update.message.reply_text(
                MessageObserver.LOCATION_CHECKED.value
            )
        logger.info(f"------Channel------ Location: {latitude} {longitude}")
    elif message.video:
        file_id = message.video.file_id
        logger.info(f"------Channel------ Video: {file_id}.mp4")
        download_link = get_download_link(BOT_TOKEN, file_id)
        images += download_link + ", "
        repair.update_cell_by(row_num=issue_cell.row, col_name="images", value=images)
