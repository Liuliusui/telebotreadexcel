from commands.limited_access.limited_access import allowed_users
from commands.message_observer.add_new_product_detection import on_reply_add_new_product_message
from commands.message_observer.issue_reply_detection import on_reply_issue_message
from config.config import logging as logger

from helpers.constants import ChannelId
from helpers.strings import LimitedAccess


async def message_observer_handler(update, context):
    message = update.message
    # Check if the message is a photo
    if message.reply_to_message:
        if message.reply_to_message.forward_from_chat.username == ChannelId.REPORT_ISSUE.value:
            await on_reply_issue_message(update=update, message=message)
        elif message.reply_to_message.forward_from_chat.username == ChannelId.ADD_NEW_PRODUCT.value:
            await on_reply_add_new_product_message(message=message)
    else:
        logger.error("This message is not a reply.")

    logger.info(f"Received message in group {message}")
