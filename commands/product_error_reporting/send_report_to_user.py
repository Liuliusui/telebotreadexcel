from commands.menu.model.new_issue_model import NewIssueModel
from commands.menu.model.new_product_model import NewProductModel
from config import bot
from helpers.strings import ProductErrorReporting, AddProduct


async def send_new_issue_message(room_id="@test_bot_long", report_data: NewIssueModel = NewIssueModel()) -> int:
    message_content = ProductErrorReporting.REPORT_SEND_MESSAGE.value.format(report_data.imei,
                                                                             report_data.customer_name,
                                                                             report_data.phone_number,
                                                                             report_data.location_description,
                                                                             report_data.location,
                                                                             report_data.description)
    sent_message = await bot.send_message(chat_id=room_id, text=message_content)
    return sent_message.message_id


async def send_new_product_message(room_id="@newproduct9499", new_product: NewProductModel = NewProductModel()) -> int:
    message_content = AddProduct.ADD_NEW_PRODUCT_MESSAGE.value.format(new_product.imei,
                                                                      new_product.name,
                                                                      new_product.model,
                                                                      new_product.customer_name,
                                                                      new_product.phone_number,
                                                                      new_product.location_desc)
    sent_message = await bot.send_message(chat_id=room_id, text=message_content)
    return sent_message.message_id
