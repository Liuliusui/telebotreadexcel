Telegram Bot
==============================

A file structure template, development environment and rule set for python data analytics projects on the data analytics team

Getting Started
------------
- Get excel online link:
    - Get download link from online google drive link
    - Set access level of folder to available for everyone
    - Currently, the share link is https://docs.google.com/spreadsheets/d/ID_OF_FILE
    - So, the download link will be https://drive.google.com/uc?export=download&id=ID_OF_FILE

Project Organization
------------

    ├── README.md          <- The top-level README for developers using this project.
    ├── env                <- The project's environment variable
    │
    ├── demo_horoscope.py              <- Demo for horoscope project which contains the demo functions that are very useful for development
    │
    ├── main.py             <- Run file use to run the bot
    │
    ├── utils          <- Functions to work with the bot


Build
--------
    1. Download python/ python3 in your devices
    2. Install pip, pandas and numpy libraries
    3. Run python3 main.py and go to telegram to experiment with the bot
    4. Run this below command to download require libraries:
        - pip3 install -r requirements.txt (Python 3)
        - pip install -r requirements.txt (Python 2)
You can also change the BOT's token to using another bot in your project

API Usage:
    - pyTelegramBotAPI: https://pytba.readthedocs.io/en/latest/index.html
    - 

Demo: 
------------
Excel online link: https://docs.google.com/spreadsheets/d/1CVj7CZLcPM4zkGZhPGcoSb7lQzQiPVZ_/edit?usp=sharing&ouid=106599054448442614654&rtpof=true&sd=true
Excel online link download: https://drive.google.com/uc?export=download&id=1CVj7CZLcPM4zkGZhPGcoSb7lQzQiPVZ_
Telet bot: @csv_xlsx_bot