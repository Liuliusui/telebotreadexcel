import time
from datetime import datetime

from commands.menu.model.new_issue_model import NewIssueModel, IssueStatus
from commands.menu.model.new_product_model import NewProductModel
from integrate.sample import machine, repair


async def save_new_product_to_server(data: NewProductModel = NewProductModel()):
    machine.insert_new_row(
        machine.make_dict_data(imei=data.imei, machine_name=data.name, model=data.model,
                               customer_name=data.customer_name,
                               customer_phone=data.phone_number, customer_location=data.location.to_string(),
                               customer_location_text=data.location_desc, images=data.images, note=data.note,
                               id_message=data.sent_message_id)
    )


async def save_new_issue_to_server(report_data: NewIssueModel = NewIssueModel()):
    current_time = datetime.now()
    # Format the current time as a string
    date_created = current_time.strftime("%Y-%m-%d %H:%M:%S")
    repair.insert_new_row(
        repair.make_dict_data(imei=report_data.imei, date_created=date_created,
                              location=report_data.location, status=IssueStatus.PENDING.value,
                              note=report_data.description, id_message=report_data.sent_message_id)
    )
