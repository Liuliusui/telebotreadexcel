#!/usr/bin/env python
# pylint: disable=unused-argument
# This program is dedicated to the public domain under the CC0 license.

"""
First, a few callback functions are defined. Then, those functions are passed to
the Application and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Example of a bot-user conversation using ConversationHandler.
Send /start to initiate the conversation.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""
from telegram import Update
from telegram.ext import (
    Application, MessageHandler, filters
)

from commands.limited_access.limited_access import set_allowed_user_handler
from commands.message_observer.message_observer import message_observer_handler
from commands.menu.menu_features import add_new_product_handler
from config.config import BOT_TOKEN


def main() -> None:
    """Run the bot."""
    # Create the Application and pass it your bot's token.
    application = Application.builder().token(BOT_TOKEN).build()

    # Add conversation handler about product error reporting
    # application.add_handler(product_error_report_handler)

    # Add conversation handler about add new product
    application.add_handler(add_new_product_handler)

    # Register new allowed user
    application.add_handler(set_allowed_user_handler)

    # Register the message handler function
    application.add_handler(MessageHandler(filters.ALL, message_observer_handler))

    # Run the bot until the user presses Ctrl-C
    application.run_polling(allowed_updates=Update.ALL_TYPES)


if __name__ == "__main__":
    main()
